﻿namespace BoutinFlegel.Areas.Blog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Blog.Models;
	using PagedList;

	public class FeaturedImagesController : Controller
	{
		private ApplicationBlogContext db = new ApplicationBlogContext();

		// GET: FeaturedImages
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Index()
		{
			return View(db.FeaturedImages.ToPagedList(40, 1));
		}

		// GET: FeaturedImages/Details/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			FeaturedImage featuredImage = db.FeaturedImages.Find(id);
			if (featuredImage == null)
			{
				return HttpNotFound();
			}

			var viewModel = new FeaturedImageViewModel()
			{
				FeaturedImage = featuredImage
			};

			return View(viewModel);
		}

		// GET: FeaturedImages/Create
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Create()
		{
			return View();
		}

		// POST: FeaturedImages/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Create([Bind(Include = "imageID,imageURL,artistName,artistURL,posted")] FeaturedImage featuredImage)
		{
			if (ModelState.IsValid)
			{

				if (featuredImage.Posted == null)
					featuredImage.Posted = DateTime.Now;

				db.FeaturedImages.Add(featuredImage);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			var viewModel = new FeaturedImageViewModel()
			{
				FeaturedImage = featuredImage
			};

			return View(viewModel);
		}

		// GET: Posts/Recent/5
		public ActionResult Recent()
		{
			if (db.FeaturedImages.Count() == 0)
			{
				return null;
			}

			var featuredImages = db.FeaturedImages.OrderByDescending(s => s.Posted).Take(5).OrderBy(g => Guid.NewGuid());

			FeaturedImage featuredImage = featuredImages.First();
			if (featuredImage == null)
			{
				return HttpNotFound();
			}

			var viewModel = new FeaturedImageViewModel()
			{
				FeaturedImage = featuredImage
			};

			return PartialView(viewModel);
		}

		// GET: FeaturedImages/Edit/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			FeaturedImage featuredImage = db.FeaturedImages.Find(id);
			if (featuredImage == null)
			{
				return HttpNotFound();
			}

			var viewModel = new FeaturedImageViewModel()
			{
				FeaturedImage = featuredImage
			};

			return View(viewModel);
		}

		// POST: FeaturedImages/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult EditFeaturesImage(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			FeaturedImage featuredImage = db.FeaturedImages.Find(id);
			if (featuredImage == null)
			{
				return HttpNotFound();
			}

			if (TryUpdateModel(featuredImage))
			{
				try
				{
					db.SaveChanges();

					return RedirectToAction("Index");
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}

				return RedirectToAction("Index");
			}

			var viewModel = new FeaturedImageViewModel()
			{
				FeaturedImage = featuredImage
			};

			return View(viewModel);
		}

		// GET: FeaturedImages/Delete/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			FeaturedImage featuredImage = db.FeaturedImages.Find(id);
			if (featuredImage == null)
			{
				return HttpNotFound();
			}

			var viewModel = new FeaturedImageViewModel()
			{
				FeaturedImage = featuredImage
			};

			return View(viewModel);
		}

		// POST: FeaturedImages/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult DeleteConfirmed(int id)
		{
			FeaturedImage featuredImage = db.FeaturedImages.Find(id);
			db.FeaturedImages.Remove(featuredImage);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
