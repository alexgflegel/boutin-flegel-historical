﻿namespace BoutinFlegel.Areas.Blog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using System.Security.Principal;
	using Microsoft.AspNet.Identity;
	using BoutinFlegel.Areas.Blog.Models;
	using HeyRed.MarkdownSharp;
	using PagedList;

	public class PostsController : Controller
	{
		private ApplicationBlogContext db = new ApplicationBlogContext();

		// GET: Posts
		public ActionResult Index(int? page, [Bind(Include = "TitleFilter,ContentFilter,StatusFilter")] PostSearchModel searchModel)
		{
			var posts = db.Posts.Select(s => s);

			if (!User.IsInRole(nameof(Roles.BlogAdmin)))
			{
				posts = posts.Where(s => s.StatusId == Post.Status.Posted);
			}

			//let the model manage filtering
			searchModel.Filter(ref posts);

			var orderByObject = posts.OrderBy(s => s.Posted);

			//there is no ThenBy(), but this is done just to be consistent with the other controllers
			int pageSize = 15;
			int pageNumber = (page ?? 1);

			var viewModel = new PostIndexViewModel()
			{
				Posts = orderByObject.ToPagedList(pageNumber, pageSize),
				Page = pageNumber,
				PostSearchModel = searchModel
			};

			return View(viewModel);
		}

		// GET: Posts/Details/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}

			var viewModel = new PostViewModel()
			{
				Post = post,
			};

			return View(viewModel);
		}

		// GET: Posts/ViewDetails/5
		public ActionResult ViewDetails(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			var post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}

			var viewModel = new PostViewModel()
			{
				Post = post
			};

			if (post.StatusId == Post.Status.Draft)
			{
				if (User.IsInRole(nameof(Roles.BlogAdmin)))
					return View(viewModel);
				else
					return HttpNotFound();
			}
			else
			{
				return View(viewModel);
			}
		}

		// GET: Posts/Details/5
		public ActionResult Recent()
		{
			if (db.Posts.Count() == 0)
			{
				return null;
			}

			var posts = db.Posts.Select(s => s);

			if (!User.IsInRole(nameof(Roles.BlogAdmin)))
			{
				posts = posts.Where(s => s.StatusId == Post.Status.Posted);
				if (posts.Count() == 0)
					return null;
			}

			var viewModel = new PostViewModel()
			{
				Post = posts.OrderByDescending(s => s.Posted).First()
			};

			return PartialView(viewModel);
		}

		// GET: Posts/Create
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Create()
		{
			var viewModel = new PostViewModel()
			{
				Categories = new SelectList(Enum.GetValues(typeof(Post.Category))),
				Statuses = new SelectList(Enum.GetValues(typeof(Post.Status)))
			};

			return View(viewModel);
		}

		// POST: Posts/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "title,featuredText,mainContent,statusID,posted,modified,categoryID")] Post post)
		{
			if (ModelState.IsValid)
			{
				//to do: need some more validation when preview is implemented
				if (!post.Posted.HasValue)
					post.Posted = DateTime.Now;
				post.Modified = DateTime.Now;

				post.UserId = User.Identity.GetUserId();

				db.Posts.Add(post);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			var viewModel = new PostViewModel()
			{
				Post = post,
				Categories = new SelectList(Enum.GetValues(typeof(Post.Category)), post.CategoryId),
				Statuses = new SelectList(Enum.GetValues(typeof(Post.Status)), post.StatusId)
			};

			return View(viewModel);
		}

		// GET: Posts/Edit/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}

			var viewModel = new PostViewModel()
			{
				Post = post,
				Categories = new SelectList(Enum.GetValues(typeof(Post.Category)), post.CategoryId),
				Statuses = new SelectList(Enum.GetValues(typeof(Post.Status)), post.StatusId)
			};

			return View(viewModel);
		}

		// POST: Posts/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		[ValidateAntiForgeryToken]
		public ActionResult EditPost(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}
			post.Modified = DateTime.Now;

			if (TryUpdateModel(post))
			{
				try
				{
					db.SaveChanges();

					return RedirectToAction("Index");
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}

				return RedirectToAction("Index");
			}

			var viewModel = new PostViewModel()
			{
				Post = post,
				Categories = new SelectList(Enum.GetValues(typeof(Post.Category)), post.CategoryId),
				Statuses = new SelectList(Enum.GetValues(typeof(Post.Status)), post.StatusId)
			};

			return View(viewModel);
		}

		// GET: Posts/Delete/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Post post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}

			var viewModel = new PostViewModel()
			{
				Post = post
			};

			return View(viewModel);
		}

		// POST: Posts/Delete/5
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			Post post = db.Posts.Find(id);
			db.Posts.Remove(post);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		[HttpGet]
		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		[ValidateInput(false)]
		public string ProcessMarkdown(string markdownText)
		{
			var mark = new Markdown();

			return mark.Transform(markdownText);
		}
	}
}
