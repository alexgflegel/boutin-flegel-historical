namespace BoutinFlegel.Areas.Blog.Models
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Diagnostics.CodeAnalysis;
	using System.Data.Entity.Spatial;
	using System.Web.Mvc;
	using System.ComponentModel;
	using PagedList;
	using System.Web.Routing;

	public class FeaturedImageIndexViewModel : FeaturedImageModel.BaseViewModel
	{
		public PagedList<FeaturedImage> FeaturedImages { get; set; }
	}

	public class FeaturedImageViewModel : FeaturedImageModel.BaseViewModel
	{
		public FeaturedImage FeaturedImage { get; set; }

		public IEnumerable<SelectListItem> Collections { get; set; }
	}

	namespace FeaturedImageModel
	{
		public class BaseViewModel
		{
			public int? Page { get; set; }
		}
	}
}
