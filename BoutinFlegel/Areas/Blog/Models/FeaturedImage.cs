namespace BoutinFlegel.Areas.Blog.Models
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("blog.FeaturedImage")]
	[DisplayName("Featured Image")]
	public partial class FeaturedImage
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ImageId { get; set; }

		[Display(Name = "Image URL")]
		public string ImageURL { get; set; }

		[StringLength(100)]
		[Display(Name = "Artist Name")]
		public string ArtistName { get; set; }

		[Display(Name = "Artist URL")]
		public string ArtistURL { get; set; }

		public DateTime? Posted { get; set; }
	}
}
