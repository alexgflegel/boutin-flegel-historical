namespace BoutinFlegel.Areas.Blog.Models
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Diagnostics.CodeAnalysis;
	using System.Data.Entity.Spatial;
	using System.Web.Mvc;
	using System.ComponentModel;

	[Table("blog.Post")]
	[DisplayName("Post")]
	public partial class Post
	{
		public enum Status
		{
			Draft = 1,
			Posted = 2,
			Archived = 3
		}

		public enum Category
		{
			Travel = 1,
			Development = 2
		}


		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Post()
		{
			Tags = new HashSet<Tag>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PostId { get; set; }

		[Required]
		[StringLength(50)]
		public string Title { get; set; }

		[AllowHtml]
		[Display(Name = "Featured")]
		public string FeaturedText { get; set; }

		[AllowHtml]
		[DataType(DataType.MultilineText)]
		[Display(Name = "Content")]
		public string MainContent { get; set; }

		[Required]
		[Display(Name = "Status")]
		public Status StatusId { get; set; }

		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? Posted { get; set; }

		public DateTime? Modified { get; set; }

		[Required]
		[Display(Name = "Category")]
		public Category CategoryId { get; set; }

		[StringLength(128)]
		public string UserId { get; set; }

		[Display(Name = "Title Text")]
		public string TitleText { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Tag> Tags { get; set; }
	}
}
