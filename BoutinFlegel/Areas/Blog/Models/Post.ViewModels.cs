namespace BoutinFlegel.Areas.Blog.Models
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Diagnostics.CodeAnalysis;
	using System.Data.Entity.Spatial;
	using System.Web.Mvc;
	using System.ComponentModel;
	using PagedList;
	using System.Web.Routing;

	public class PostIndexViewModel : PostModel.BaseViewModel
	{
		public IPagedList<Post> Posts { get; set; }
	}

	public class PostViewModel : PostModel.BaseViewModel
	{
		public Post Post { get; set; }

		public IEnumerable<SelectListItem> Categories { get; set; }
		public IEnumerable<SelectListItem> Statuses { get; set; }
	}

	namespace PostModel
	{
		public class BaseViewModel
		{
			public PostSearchModel PostSearchModel { get; set; }

			public int? Page { get; set; }

			public RouteValueDictionary RouteValues(RouteValueDictionary ExternalValues)
			{
				return PostSearchModel.RouteValues.Extend(ExternalValues);
			}
		}
	}
}
