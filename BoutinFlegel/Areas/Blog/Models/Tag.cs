namespace BoutinFlegel.Areas.Blog.Models
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Diagnostics.CodeAnalysis;
	using System.Data.Entity.Spatial;
	using System.ComponentModel;

	[Table("blog.Tag")]
	[DisplayName("Tag")]
	public partial class Tag
	{
		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Tag()
		{
			Posts = new HashSet<Post>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int TagId { get; set; }

		[Required]
		[StringLength(50)]
		public string Name { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Post> Posts { get; set; }
	}
}
