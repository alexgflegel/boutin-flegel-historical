namespace BoutinFlegel.Areas.Blog.Models
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class ApplicationBlogContext : DbContext
	{
		public ApplicationBlogContext() : base("name=ApplicationContext") { }

		public virtual DbSet<FeaturedImage> FeaturedImages { get; set; }
		public virtual DbSet<Post> Posts { get; set; }
		public virtual DbSet<Tag> Tags { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Post>()
				.HasMany(e => e.Tags)
				.WithMany(e => e.Posts)
				.Map(m => m.ToTable("TagsInPosts", "blog").MapLeftKey("postID").MapRightKey("tagID"));
		}
	}
}
