﻿using System.Linq;
using System.Web.Routing;

namespace BoutinFlegel.Areas.Blog.Models
{
	/// <summary>
	/// Manages the routing values and filtering for the post search
	/// </summary>
	public class PostSearchModel : BaseSearchModel
	{
		public string TitleFilter { get; set; }

		/// <summary>
		/// Returns the RouteValueDictionary for all parts of the search object
		/// </summary>
		public override RouteValueDictionary RouteValues => new RouteValueDictionary { [nameof(TitleFilter)] = TitleFilter };

		/// <summary>
		/// Filters a dataset for each of the search models parameters
		/// </summary>
		/// <param name="posts"></param>
		public void Filter(ref IQueryable<Post> posts)
		{
			if (string.IsNullOrWhiteSpace(TitleFilter))
				return;

			foreach (var keyword in BuildKeywords(TitleFilter))
			{
				if (string.IsNullOrWhiteSpace(keyword.Value))
				{
					posts = posts.Where(s => s.Title.Contains(keyword.Value) ||
						s.MainContent.Contains(keyword.Value) ||
						s.TitleText.Contains(keyword.Value));
				}
				else if (keyword.Key == "title")
				{
					posts = posts.Where(s => s.Title.Contains(keyword.Value));
				}
				else if (keyword.Key == "body")
				{
					posts = posts.Where(s => s.MainContent.Contains(keyword.Value));
				}
				else if (keyword.Key == "featured")
				{
					posts = posts.Where(s => s.TitleText.Contains(keyword.Value));
				}
			}
		}
	}
}
