﻿namespace BoutinFlegel.Areas.Catalog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using PagedList;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Catalog.Models;
	using System.Web.Routing;

	[Authorize(Roles = nameof(Roles.CatalogAdmin))]
	public class InventoryLotsController : Controller
	{
		private ApplicationCatalogContext db = new ApplicationCatalogContext();

		// GET: InventoryLots
		public ActionResult Index()
		{
			var inventoryLots = db.InventoryLots.Include(i => i.Inventory);
			return View(new InventoryLotIndexViewModel() { InventoryLots = inventoryLots.ToPagedList(40, 1) });
		}

		// GET: InventoryLots/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			InventoryLot inventoryLot = db.InventoryLots.Find(id);
			if (inventoryLot == null)
			{
				return HttpNotFound();
			}

			var viewModel = new InventoryLotViewModel()
			{
				Inventories = new SelectList(db.Inventories.Where(inventory => inventory.CurrencyDivisionId == inventoryLot.Inventory.CurrencyDivisionId)
					.Select(s => new
					{
						inventoryID = s.InventoryId,
						concatenate = string.Format("{0}, {1} - {2}", s.Year, s.DenominationAlternate.NullIfWhiteSpace() ?? s.Denomination.ToString(), s.Description)
					}
				), "inventoryID", "concatenate", inventoryLot.InventoryId)
			};

			return View(viewModel);
		}

		// GET: InventoryLots/Create
		public ActionResult Create(int? inventoryID, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			if (inventoryID.HasValue)
			{
				db.InventoryLots.Add(new InventoryLot()
				{
					InventoryId = inventoryID.Value,
					Grade = "N/A"
				});

				db.SaveChanges();
				return RedirectToAction("Details", "Inventories", searchModel.RouteValues.Extend(new RouteValueDictionary(new { id = inventoryID, Page = Page })));
			}

			var viewModel = new InventoryLotViewModel()
			{
				Countries = Extensions.SelectList<Country>(db.Countries),
				CountryDivisions = Extensions.SelectList<CountryDivision>(new Dictionary<string, string>()),
				Currencies = Extensions.SelectList<Currency>(new Dictionary<string, string>()),
				CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(new Dictionary<string, string>()),

				Inventories = new SelectList(new Dictionary<string, string>(), "inventoryID", "concatenate", null)
			};
			return View(viewModel);
		}

		// POST: InventoryLots/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "inventoryLotID,grade,inventoryID,verified")] InventoryLot inventoryLot)
		{
			if (ModelState.IsValid)
			{
				db.InventoryLots.Add(inventoryLot);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			var viewModel = new InventoryLotViewModel()
			{
				InventoryLot = inventoryLot,

				Countries = Extensions.SelectList<Country>(db.Countries),
				CountryDivisions = Extensions.SelectList<CountryDivision>(new Dictionary<string, string>()),
				Currencies = Extensions.SelectList<Currency>(new Dictionary<string, string>()),
				CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(new Dictionary<string, string>()),

				Inventories = new SelectList(new Dictionary<string, string>(), "inventoryID", "concatenate", null)
			};
			return View(viewModel);
		}

		// GET: InventoryLots/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			InventoryLot inventoryLot = db.InventoryLots.Find(id);
			if (inventoryLot == null)
			{
				return HttpNotFound();
			}

			var viewModel = new InventoryLotViewModel()
			{
				InventoryLot = inventoryLot,

				Countries = Extensions.SelectList<Country>(db.Countries),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == inventoryLot.Inventory.CurrencyDivision.Currency.CountryDivision.CountryId)),
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CurrencyId == inventoryLot.Inventory.CurrencyDivision.CurrencyId)),
				CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(db.CurrencyDivisions.Where(currencyDivision => currencyDivision.CurrencyDivisionId == inventoryLot.Inventory.CurrencyDivisionId)),

				Inventories = new SelectList(db.Inventories.Where(inventory => inventory.CurrencyDivisionId == inventoryLot.Inventory.CurrencyDivisionId)
					.Select(s => new
					{
						inventoryID = s.InventoryId,
						concatenate = s.Year.ToString() + ", " + s.Denomination.ToString() + " - " + s.Description
					}
				), "inventoryID", "concatenate", inventoryLot.InventoryId)
			};
			return View(viewModel);
		}

		// POST: InventoryLots/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditInventoryLot(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			InventoryLot inventoryLot = db.InventoryLots.Find(id);
			if (inventoryLot == null)
			{
				return HttpNotFound();
			}

			if (TryUpdateModel(inventoryLot))
			{
				try
				{
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}
				return RedirectToAction("Index");
			}

			var viewModel = new InventoryLotViewModel()
			{
				InventoryLot = inventoryLot,

				Countries = Extensions.SelectList<Country>(db.Countries),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == inventoryLot.Inventory.CurrencyDivision.Currency.CountryDivision.CountryId)),
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CurrencyId == inventoryLot.Inventory.CurrencyDivision.CurrencyId)),
				CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(db.CurrencyDivisions.Where(currencyDivision => currencyDivision.CurrencyDivisionId == inventoryLot.Inventory.CurrencyDivisionId)),

				Inventories = new SelectList(db.Inventories.Where(inventory => inventory.CurrencyDivisionId == inventoryLot.Inventory.CurrencyDivisionId)
					.Select(s => new
					{
						inventoryID = s.InventoryId,
						concatenate = s.Year.ToString() + ", " + s.Denomination.ToString() + " - " + s.Description
					}
				), "inventoryID", "concatenate", inventoryLot.InventoryId)
			};
			return View(viewModel);
		}

		// GET: InventoryLots/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			InventoryLot inventoryLot = db.InventoryLots.Find(id);
			if (inventoryLot == null)
			{
				return HttpNotFound();
			}

			var viewModel = new InventoryLotViewModel()
			{
				InventoryLot = inventoryLot,
			};
			return View(viewModel);
		}

		// POST: InventoryLots/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			InventoryLot inventoryLot = db.InventoryLots.Find(id);
			db.InventoryLots.Remove(inventoryLot);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		// POST: InventoryLots/Filter/5
		// Returns InventoryLots by Inventory
		public ActionResult Filter(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var inventoryLots = db.InventoryLots.Where(division => division.InventoryId == id);
			if (inventoryLots == null)
			{
				return HttpNotFound();
			}

			return Json(new SelectList(inventoryLots, "inventoryLotID", "name"));
		}
	}
}
