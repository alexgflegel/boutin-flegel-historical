﻿namespace BoutinFlegel.Areas.Catalog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Catalog.Models;
	using PagedList;
	using System.Web.Routing;
	using System.ComponentModel.DataAnnotations;

	[Authorize(Roles = nameof(Roles.CatalogAdmin))]
	public class InventoriesController : Controller
	{
		private ApplicationCatalogContext db = new ApplicationCatalogContext();

		// GET: Inventories
		public ActionResult Index(int? page, InventorySearchModel.Sort? sortOrder, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			var inventory = db.Inventories.Include(i => i.CurrencyDivision);

			var viewModel = new InventoryIndexViewModel()
			{
				InventorySort = sortOrder ?? InventorySearchModel.Sort.NameAscending,
				InventorySearchModel = searchModel,
				CollectionSort = InventorySearchModel.Sort.CollectionAscending,
				CountrySort = InventorySearchModel.Sort.NameAscending
			};

			//run a filter based on the passed in objects
			searchModel.Filter(ref inventory);


			//store the partially default sorted list of objects
			var inventories = inventory.OrderBy(s => s.CurrencyDivision.Currency.CountryDivision.Country.Name);

			switch (viewModel.InventorySort)
			{
				case InventorySearchModel.Sort.NameAscending:
					inventories = inventory.OrderBy(s => s.CurrencyDivision.Currency.CountryDivision.Country.Name);
					viewModel.CountrySort = InventorySearchModel.Sort.NameDescending;
					break;
				case InventorySearchModel.Sort.NameDescending:
					inventories = inventory.OrderByDescending(s => s.CurrencyDivision.Currency.CountryDivision.Country.Name);
					viewModel.CountrySort = InventorySearchModel.Sort.NameAscending;
					break;
				case InventorySearchModel.Sort.CollectionAscending:
					inventories = inventory.OrderBy(s => s.Collection.Name);
					viewModel.CollectionSort = InventorySearchModel.Sort.CollectionDescending;
					break;
				case InventorySearchModel.Sort.CollectionDescending:
					inventories = inventory.OrderByDescending(s => s.Collection.Name);
					viewModel.CollectionSort = InventorySearchModel.Sort.CollectionAscending;
					break;
				default:
					inventories = inventory.OrderBy(s => s.CurrencyDivision.Currency.CountryDivision.Country.Name);
					break;
			}

			//PagedList variables
			int pageSize = 40;
			int pageNumber = (page ?? 1);
			viewModel.Page = pageNumber;

			viewModel.Inventories = inventories
				.ThenBy(s => s.CurrencyDivision.Currency.Name)
				.ThenBy(s => s.Collection.Name)
				.ThenByDescending(s => s.TypeId)
				.ThenByDescending(s => s.CurrencyDivision.Division)
				.ThenBy(s => s.Denomination).ThenBy(s => s.Year).ToPagedList(pageNumber, pageSize);

			return View(viewModel);
		}

		// GET: Inventories/Details/5
		public ActionResult Details(int? id, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Inventory inventory = db.Inventories.Find(id);
			if (inventory == null)
			{
				return HttpNotFound();
			}

			var viewModel = new InventoryViewModel()
			{
				Page = Page,
				InventorySearchModel = searchModel,
			};

			viewModel.Inventory = inventory;
			return View(viewModel);
		}

		// GET: Inventories/Create
		public ActionResult Create(int? parentId, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			var viewModel = new InventoryViewModel()
			{
				Page = Page,
				InventorySearchModel = searchModel,

				Collections = Extensions.SelectList<Collection>(db.Collections),
				Mediums = new SelectList(Enum.GetValues(typeof(Inventory.Medium)))
			};

			if (parentId.HasValue)
			{
				var parentCurrencyDivision = db.CurrencyDivisions.Find(parentId);

				//grab the parent item information
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries, parentCurrencyDivision.Currency.CountryDivision.CountryId);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == parentCurrencyDivision.Currency.CountryDivision.CountryId), parentCurrencyDivision.Currency.CountryDivisionId);
				viewModel.Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == parentCurrencyDivision.Currency.CountryDivisionId), parentCurrencyDivision.CurrencyId);
				viewModel.CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(db.CurrencyDivisions.Where(currencyDivision => currencyDivision.CurrencyId == parentCurrencyDivision.CurrencyId), parentId);
			}
			else
			{
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(new Dictionary<string, string>());
				viewModel.Currencies = Extensions.SelectList<Currency>(new Dictionary<string, string>());
				viewModel.CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(new Dictionary<string, string>());
			}

			return View(viewModel);
		}

		// POST: Inventories/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = Inventory.BindString)] Inventory inventory, int? countryID, int? countryDivisionID, int? currencyID, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			if (ModelState.IsValid)
			{
				db.Inventories.Add(inventory);
				db.SaveChanges();


				InventoryLot inventoryLot = new InventoryLot()
				{
					InventoryId = inventory.InventoryId,
					Grade = "N/A"
				};

				db.InventoryLots.Add(inventoryLot);
				db.SaveChanges();

				return RedirectToAction("Index", searchModel.RouteValues.Extend(new RouteValueDictionary(new { Page = Page })));
			}

			var viewModel = new InventoryViewModel()
			{
				Page = Page,
				Inventory = inventory,
				InventorySearchModel = searchModel,
			};

			//this populates the drop-downs from the bottom up
			if (currencyID.HasValue)
			{
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries, countryID);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == countryID), countryDivisionID);
				viewModel.Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == countryDivisionID), currencyID);
				viewModel.CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(db.CurrencyDivisions.Where(currencyDivision => currencyDivision.CurrencyId == currencyID));
			}
			else
			{
				viewModel.CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(new Dictionary<string, string>());

				if (countryDivisionID.HasValue)
				{
					viewModel.Countries = Extensions.SelectList<Country>(db.Countries, countryID);
					viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == countryID), countryDivisionID);
					viewModel.Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == countryDivisionID));
				}
				else
				{
					viewModel.Currencies = Extensions.SelectList<Currency>(new Dictionary<string, string>());

					if (countryID.HasValue)
					{
						viewModel.Countries = Extensions.SelectList<Country>(db.Countries, countryID);
						viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == countryID));
					}
					else
					{
						viewModel.Countries = Extensions.SelectList<Country>(db.Countries, countryID);
						viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(new Dictionary<string, string>());
					}
				}
			}

			viewModel.Collections = new SelectList(db.Collections, "collectionID", "name", inventory.CollectionId);
			viewModel.Mediums = new SelectList(Enum.GetValues(typeof(Inventory.Medium)), inventory.TypeId);
			return View(viewModel);
		}

		// GET: Inventories/Edit/5
		public ActionResult Edit(int? id, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{


			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Inventory inventory = db.Inventories.Find(id);
			if (inventory == null)
			{
				return HttpNotFound();
			}

			var viewModel = new InventoryViewModel()
			{
				Page = Page,
				Inventory = inventory,
				InventorySearchModel = searchModel,

				Countries = Extensions.SelectList<Country>(db.Countries, inventory.CurrencyDivision.Currency.CountryDivision.CountryId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == inventory.CurrencyDivision.Currency.CountryDivision.CountryId), inventory.CurrencyDivision.Currency.CountryDivisionId),
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == inventory.CurrencyDivision.Currency.CountryDivisionId), inventory.CurrencyDivision.CurrencyId),
				CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(db.CurrencyDivisions.Where(currencyDivision => currencyDivision.CurrencyId == inventory.CurrencyDivision.CurrencyId), inventory.CurrencyDivisionId),
				Collections = Extensions.SelectList<Collection>(db.Collections),
				Mediums = new SelectList(Enum.GetValues(typeof(Inventory.Medium)))
			};

			return View(viewModel);
		}

		// POST: Inventories/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditInventory(int? id, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Inventory inventory = db.Inventories.Find(id);
			if (inventory == null)
			{
				return HttpNotFound();
			}

			if (TryUpdateModel(inventory))
			{
				try
				{
					db.SaveChanges();
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}

				return RedirectToAction("Index", searchModel.RouteValues.Extend(new RouteValueDictionary(new { Page = Page })));
			}

			var viewModel = new InventoryViewModel()
			{
				Page = Page,
				Inventory = inventory,
				InventorySearchModel = searchModel,

				Countries = Extensions.SelectList<Country>(db.Countries, inventory.CurrencyDivision.Currency.CountryDivision.CountryId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == inventory.CurrencyDivision.Currency.CountryDivision.CountryId), inventory.CurrencyDivision.Currency.CountryDivisionId),
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == inventory.CurrencyDivision.Currency.CountryDivisionId), inventory.CurrencyDivision.CurrencyId),
				CurrencyDivisions = Extensions.SelectList<CurrencyDivision>(db.CurrencyDivisions.Where(currencyDivision => currencyDivision.CurrencyId == inventory.CurrencyDivision.CurrencyId), inventory.CurrencyDivisionId),
				Collections = Extensions.SelectList<Collection>(db.Collections),
				Mediums = new SelectList(Enum.GetValues(typeof(Inventory.Medium)))
			};
			return View(viewModel);
		}

		// GET: Inventories/Delete/5
		public ActionResult Delete(int? id, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{

			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Inventory inventory = db.Inventories.Find(id);
			if (inventory == null)
			{
				return HttpNotFound();
			}
			var viewModel = new InventoryViewModel()
			{
				Page = Page,
				Inventory = inventory,
				InventorySearchModel = searchModel
			};

			return View(viewModel);
		}

		// POST: Inventories/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id, int? Page, [Bind(Include = InventorySearchModel.BindString)] InventorySearchModel searchModel)
		{
			//find and remove the newly orphaned inventory lots
			var inventoryLot = db.InventoryLots.Where(s => s.InventoryId == id);

			foreach (InventoryLot toDelete in inventoryLot)
			{
				db.InventoryLots.Remove(toDelete);
			}

			Inventory inventory = db.Inventories.Find(id);
			db.Inventories.Remove(inventory);
			db.SaveChanges();
			return RedirectToAction("Index", searchModel.RouteValues.Extend(new RouteValueDictionary(new { Page = Page })));
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		// POST: Inventories/Filter/5
		// Returns Inventories by CurrencyDivision
		public ActionResult Filter(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			var inventory = db.Inventories.Where(division => division.CurrencyDivisionId == id)
				.Select(s => new
				{
					inventoryID = s.InventoryId,
					concatenate = s.Year.ToString() + ", " + s.Denomination.ToString() + " - " + s.Description
				});

			if (inventory == null)
			{
				return HttpNotFound();
			}

			return Json(new SelectList(inventory, "inventoryID", "concatenate"));
		}
	}
}
