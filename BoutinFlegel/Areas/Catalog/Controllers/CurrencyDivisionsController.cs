﻿namespace BoutinFlegel.Areas.Catalog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Catalog.Models;
	using PagedList;

	[Authorize(Roles = nameof(Roles.CatalogAdmin))]
	public class CurrencyDivisionsController : Controller
	{
		private ApplicationCatalogContext db = new ApplicationCatalogContext();

		// GET: CurrencyDivisions
		public ActionResult Index(int? page)
		{
			var currencyDivisions = db.CurrencyDivisions.Include(c => c.Currency);

			IOrderedQueryable<CurrencyDivision> orderByObject = currencyDivisions.OrderBy(s => s.Currency.CountryDivision.Country.Name);

			currencyDivisions = orderByObject.ThenBy(s => s.Currency.Name).ThenBy(s => s.Division);

			int pageSize = 40;
			int pageNumber = (page ?? 1);

			var viewModel = new CurrencyDivisionIndexViewModel()
			{
				Page = pageNumber,
				CurrencyDivisions = currencyDivisions.ToPagedList(pageNumber, pageSize)
			};

			return View(viewModel);
		}

		// GET: CurrencyDivisions/Details/5
		public ActionResult Details(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CurrencyDivision currencyDivision = db.CurrencyDivisions.Find(id);
			if (currencyDivision == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CurrencyDivisionViewModel()
			{
				Page = Page,
				CurrencyDivision = currencyDivision
			};

			return View(viewModel);
		}

		// GET: CurrencyDivisions/Create
		public ActionResult Create(int? parentId, int? Page)
		{
			var viewModel = new CurrencyDivisionViewModel()
			{
				Page = Page,
			};

			if (parentId.HasValue)
			{
				Currency parentCurrency = db.Currencies.Find(parentId);

				//grab the parent item information
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries, parentCurrency.CountryDivision.CountryId);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == parentCurrency.CountryDivision.CountryId), parentCurrency.CountryDivisionId);
				viewModel.Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == parentCurrency.CountryDivisionId), parentId);
			}
			else
			{
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(new Dictionary<string, string>());
				viewModel.Currencies = Extensions.SelectList<Currency>(new Dictionary<string, string>());
			}

			return View(viewModel);
		}

		// POST: CurrencyDivisions/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "currencyDivisionID,legacy_id,currencyID,nameSingular,namePlural,localizationSingular,localizationPlural,symbol,division")] CurrencyDivision currencyDivision, int? Page)
		{
			if (ModelState.IsValid)
			{
				db.CurrencyDivisions.Add(currencyDivision);
				db.SaveChanges();
				return RedirectToAction("Create", "Inventories", new { parentId = currencyDivision.CurrencyDivisionId });
			}

			var viewModel = new CurrencyDivisionViewModel()
			{
				Page = Page,
				CurrencyDivision = currencyDivision,
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == currencyDivision.Currency.CountryDivisionId), currencyDivision.CurrencyId)
			};

			return View(viewModel);
		}

		// GET: CurrencyDivisions/Edit/5
		public ActionResult Edit(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CurrencyDivision currencyDivision = db.CurrencyDivisions.Find(id);
			if (currencyDivision == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CurrencyDivisionViewModel()
			{
				Page = Page,
				CurrencyDivision = currencyDivision,
				Countries = Extensions.SelectList<Country>(db.Countries, currencyDivision.Currency.CountryDivision.CountryId),
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == currencyDivision.Currency.CountryDivisionId), currencyDivision.CurrencyId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == currencyDivision.Currency.CountryDivisionId), currencyDivision.CurrencyId)
			};

			return View(viewModel);
		}

		// POST: CurrencyDivisions/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditCurrencyDivision(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CurrencyDivision currencyDivision = db.CurrencyDivisions.Find(id);
			if (currencyDivision == null)
			{
				return HttpNotFound();
			}

			if (TryUpdateModel(currencyDivision))
			{
				try
				{
					db.SaveChanges();
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}

				return RedirectToAction("Index", new { page = Page });
			}

			var viewModel = new CurrencyDivisionViewModel()
			{
				Page = Page,
				CurrencyDivision = currencyDivision,
				Countries = Extensions.SelectList<Country>(db.Countries, currencyDivision.Currency.CountryDivision.CountryId),
				Currencies = Extensions.SelectList<Currency>(db.Currencies.Where(currency => currency.CountryDivisionId == currencyDivision.Currency.CountryDivisionId), currencyDivision.CurrencyId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == currencyDivision.Currency.CountryDivisionId), currencyDivision.CurrencyId)
			};

			return View(viewModel);
		}

		// GET: CurrencyDivisions/Delete/5
		public ActionResult Delete(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CurrencyDivision currencyDivision = db.CurrencyDivisions.Find(id);
			if (currencyDivision == null)
			{
				return HttpNotFound();
			}


			var viewModel = new CurrencyDivisionViewModel()
			{
				Page = Page,
				CurrencyDivision = currencyDivision,
			};

			return View(viewModel);
		}

		// POST: CurrencyDivisions/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id, int? Page)
		{
			CurrencyDivision currencyDivision = db.CurrencyDivisions.Find(id);
			db.CurrencyDivisions.Remove(currencyDivision);
			db.SaveChanges();
			return RedirectToAction("Index", new { Page = Page });
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		// POST: CurrencyDivisions/Filter/5
		// Returns CurrencyDivisions by Currency
		public ActionResult Filter(int? id)
		{
			if (id == null)
			{
				return Json(string.Empty);
			}
			var currencyDivisions = db.CurrencyDivisions.Where(division => division.CurrencyId == id);
			if (currencyDivisions == null)
			{
				return HttpNotFound();
			}

			return Json(Extensions.SelectList<CurrencyDivision>(currencyDivisions));
		}
	}
}
