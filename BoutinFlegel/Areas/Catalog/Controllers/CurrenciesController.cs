﻿namespace BoutinFlegel.Areas.Catalog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Catalog.Models;
	using PagedList;

	[Authorize(Roles = nameof(Roles.CatalogAdmin))]
	public class CurrenciesController : Controller
	{
		private ApplicationCatalogContext db = new ApplicationCatalogContext();

		// GET: Currencies
		public ActionResult Index(int? page)
		{
			//fetch and order the currencies
			var currencies = db.Currencies.Include(c => c.CountryDivision);

			currencies = currencies.OrderBy(s => s.CountryDivision.Country.Name).ThenBy(s => s.CountryDivision.Name);

			//set up the paged list information
			int pageSize = 40;
			int pageNumber = (page ?? 1);

			var viewModel = new CurrencyIndexViewModel()
			{
				Page = pageNumber,
				Currencies = currencies.ToPagedList(pageNumber, pageSize)
			};

			return View(viewModel);
		}

		// GET: Currencies/Details/5
		public ActionResult Details(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Currency currency = db.Currencies.Find(id);

			if (currency == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CurrencyViewModel()
			{
				Page = Page,
				Currency = currency
			};

			return View(viewModel);
		}

		// GET: Currencies/Create
		public ActionResult Create(int? parentId, int? Page)
		{
			var viewModel = new CurrencyViewModel()
			{
				Page = Page,
			};

			if (parentId.HasValue)
			{
				CountryDivision parentCountryDivision = db.CountryDivisions.Find(parentId);

				//grab the parent item information
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries, parentCountryDivision.CountryId);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == parentCountryDivision.CountryId), parentId);
			}
			else
			{
				viewModel.Countries = Extensions.SelectList<Country>(db.Countries);
				viewModel.CountryDivisions = Extensions.SelectList<CountryDivision>(new Dictionary<string, string>());
			}

			return View(viewModel);
		}

		// POST: Currencies/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "countryDivisionID,name,localization,symbol,abbreviation,introduced,replaced")] Currency currency, int? Page)
		{
			if (ModelState.IsValid)
			{
				db.Currencies.Add(currency);
				db.SaveChanges();
				return RedirectToAction("Create", "CurrencyDivisions", new { parentId = currency.CurrencyId });
			}

			var viewModel = new CurrencyViewModel()
			{
				Page = Page,
				Currency = currency,
				Countries = Extensions.SelectList<Country>(db.Countries, currency.CountryDivision.CountryId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == currency.CountryDivision.CountryId), currency.CountryDivisionId)

			};

			return View(viewModel);
		}

		// GET: Currencies/Edit/5
		public ActionResult Edit(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Currency currency = db.Currencies.Find(id);
			if (currency == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CurrencyViewModel()
			{
				Page = Page,
				Currency = currency,
				Countries = Extensions.SelectList<Country>(db.Countries, currency.CountryDivision.CountryId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == currency.CountryDivision.CountryId), currency.CountryDivisionId)

			};

			return View(viewModel);
		}

		// POST: Currencies/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditCurrency(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Currency currency = db.Currencies.Find(id);
			if (currency == null)
			{
				return HttpNotFound();
			}

			if (TryUpdateModel(currency))
			{
				try
				{
					db.SaveChanges();
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}

				return RedirectToAction("Index", new { Page = Page });
			}

			var viewModel = new CurrencyViewModel()
			{
				Page = Page,
				Currency = currency,
				Countries = Extensions.SelectList<Country>(db.Countries, currency.CountryDivision.CountryId),
				CountryDivisions = Extensions.SelectList<CountryDivision>(db.CountryDivisions.Where(countryDivision => countryDivision.CountryId == currency.CountryDivision.CountryId), currency.CountryDivisionId)

			};

			return View(viewModel);
		}

		// GET: Currencies/Delete/5
		public ActionResult Delete(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Currency currency = db.Currencies.Find(id);
			if (currency == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CurrencyViewModel()
			{
				Page = Page,
				Currency = currency,
			};

			return View(viewModel);
		}

		// POST: Currencies/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id, int? Page)
		{
			Currency currency = db.Currencies.Find(id);
			db.Currencies.Remove(currency);
			db.SaveChanges();
			return RedirectToAction("Index", new { Page = Page });
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		// POST: Currencies/Filter/5
		// Returns Currencies by Country Division
		public ActionResult Filter(int? id)
		{
			if (id == null)
			{
				return Json(string.Empty);
			}
			var currency = db.Currencies.Where(division => division.CountryDivisionId == id);
			if (currency == null)
			{
				return HttpNotFound();
			}

			return Json(Extensions.SelectList<Currency>(currency));
		}
	}
}
