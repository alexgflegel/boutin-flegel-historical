﻿namespace BoutinFlegel.Areas.Catalog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Catalog.Models;
	using PagedList;

	[Authorize(Roles = nameof(Roles.CatalogAdmin))]
	public class CountryDivisionsController : Controller
	{
		private ApplicationCatalogContext db = new ApplicationCatalogContext();

		// GET: CountryDivisions
		public ActionResult Index(int? page)
		{
			var countryDivisions = db.CountryDivisions.Include(c => c.Country);

			var orderByObject = countryDivisions.OrderBy(s => s.Country.Name);

			countryDivisions = orderByObject.ThenBy(s => s.Name);

			int pageSize = 40;
			int pageNumber = (page ?? 1);

			var viewModel = new CountryDivisionIndexViewModel()
			{
				Page = pageNumber,
				CountryDivisions = countryDivisions.ToPagedList(pageNumber, pageSize)
			};


			return View(viewModel);
		}

		// GET: CountryDivisions/Details/5
		public ActionResult Details(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CountryDivision countryDivision = db.CountryDivisions.Find(id);
			if (countryDivision == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CountryDivisionViewModel()
			{
				Page = Page,
				CountryDivision = countryDivision
			};

			return View(viewModel);
		}

		// GET: CountryDivisions/Create
		public ActionResult Create(int? parentId, int? Page)
		{
			var viewModel = new CountryDivisionViewModel()
			{
				Page = Page,
				Countries = Extensions.SelectList<Country>(db.Countries)
			};

			return View(viewModel);
		}

		// POST: CountryDivisions/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "countryDivisionID,countryID,name,localization,confederation,disintegration")] CountryDivision countryDivision, int? Page)
		{

			if (ModelState.IsValid)
			{
				db.CountryDivisions.Add(countryDivision);
				db.SaveChanges();
				return RedirectToAction("Create", "Currencies", new { parentId = countryDivision.CountryId });
			}

			var viewModel = new CountryDivisionViewModel()
			{
				Page = Page,
				CountryDivision = countryDivision,
				Countries = Extensions.SelectList<Country>(db.Countries, countryDivision.CountryDivisionId)
			};

			return View(viewModel);
		}

		// GET: CountryDivisions/Edit/5
		public ActionResult Edit(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CountryDivision countryDivision = db.CountryDivisions.Find(id);
			if (countryDivision == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CountryDivisionViewModel()
			{
				Page = Page,
				CountryDivision = countryDivision,
				Countries = Extensions.SelectList<Country>(db.Countries, countryDivision.CountryDivisionId)
			};

			return View(viewModel);
		}

		// POST: CountryDivisions/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditCountryDivision(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CountryDivision countryDivision = db.CountryDivisions.Find(id);
			if (countryDivision == null)
			{
				return HttpNotFound();
			}

			if (TryUpdateModel(countryDivision))
			{
				try
				{
					db.Entry(countryDivision).State = EntityState.Modified;

					db.SaveChanges();
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}

				return RedirectToAction("Index", new { Page = Page });
			}

			var viewModel = new CountryDivisionViewModel()
			{
				Page = Page,
				CountryDivision = countryDivision,
				Countries = Extensions.SelectList<Country>(db.Countries, countryDivision.CountryDivisionId)
			};

			return View(viewModel);
		}

		// GET: CountryDivisions/Delete/5
		public ActionResult Delete(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CountryDivision countryDivision = db.CountryDivisions.Find(id);
			if (countryDivision == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CountryDivisionViewModel()
			{
				Page = Page,
				CountryDivision = countryDivision,
				Countries = Extensions.SelectList<Country>(db.Countries, countryDivision.CountryDivisionId)
			};

			return View(viewModel);
		}

		// POST: CountryDivisions/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id, int? Page)
		{
			CountryDivision countryDivision = db.CountryDivisions.Find(id);
			db.CountryDivisions.Remove(countryDivision);
			db.SaveChanges();
			return RedirectToAction("Index", new { Page = Page });
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}


		// POST: CountryDivisions/Filter/5
		// Returns CountryDivisions by Country
		public ActionResult Filter(int? id)
		{
			if (id == null)
			{
				return Json(string.Empty);
			}
			var countryDivisions = db.CountryDivisions.Where(division => division.CountryId == id);
			if (countryDivisions == null)
			{
				return HttpNotFound();
			}

			return Json(Extensions.SelectList<CountryDivision>(countryDivisions));
		}
	}
}
