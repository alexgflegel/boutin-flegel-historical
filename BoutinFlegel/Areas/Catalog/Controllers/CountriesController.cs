﻿namespace BoutinFlegel.Areas.Catalog.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.Entity;
	using System.Linq;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;
	using BoutinFlegel.Areas.Catalog.Models;
	using PagedList;
	using System.Text.RegularExpressions;

	[Authorize(Roles = nameof(Roles.CatalogAdmin))]
	public class CountriesController : Controller
	{
		private ApplicationCatalogContext db = new ApplicationCatalogContext();

		// GET: Countries
		public ActionResult Index(int? Page, string sortOrder)
		{
			var countries = db.Countries.Select(s => s);


			ViewBag.CurrentSort = sortOrder;

			IOrderedQueryable<Country> orderByObject = countries.OrderBy(s => s.Name);

			//there is no ThenBy(), but this is done just to be consistent with the other controllers
			countries = orderByObject;

			switch (sortOrder)
			{
				default:
					break;
			}

			int pageSize = 40;
			int pageNumber = (Page ?? 1);
			var viewModel = new CountryIndexViewModel()
			{
				Page = pageNumber,
				Countries = countries.ToPagedList(pageNumber, pageSize)
			};

			return View(viewModel);
		}

		// GET: Countries/Details/5
		public ActionResult Details(int? id, int? Page)
		{

			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Country country = db.Countries.Find(id);
			if (country == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CountryViewModel()
			{
				Page = Page,
				Country = country
			};

			return View(viewModel);
		}

		// GET: Countries/Create
		public ActionResult Create(int? Page)
		{
			var viewModel = new CountryViewModel()
			{
				Page = Page,
			};

			return View(viewModel);
		}

		// POST: Countries/Create
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(CountryViewModel model)
		{
			if (ModelState.IsValid)
			{
				db.Countries.Add(model.Country);
				db.SaveChanges();
				return RedirectToAction("Create", "CountryDivisions", new { parentId = model.Country.CountryId });
			}


			return View(model);
		}

		// GET: Countries/Edit/5
		public ActionResult Edit(int? id, int? Page)
		{
			Country country = null;
			if (id != null)
			{
				country = db.Countries.Find(id);
				if (country == null)
					return HttpNotFound();
			}

			var viewModel = new CountryViewModel()
			{
				Page = Page,
				Country = country
			};

			return View(viewModel);
		}

		// POST: Countries/Edit/5
		// To protect from over-posting attacks, please enable the specific properties you want to bind to, for
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost, ActionName("Edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditCountry(CountryViewModel model)
		{

			if (ModelState.IsValid)
			{
				try
				{
					//UpdateModel(model.Country);
					db.Entry(model.Country).State = EntityState.Modified;
					db.SaveChanges();
					return RedirectToAction("Index", "Countries", routeValues: new { Page = model.Page });
				}
				catch (DataException)
				{
					ModelState.AddModelError("Database Error", "Unable to save changes.");
				}
			}

			//if (TryUpdateModel(model.Country))
			//{

			//}

			return View(model);
		}

		// GET: Countries/Delete/5
		public ActionResult Delete(int? id, int? Page)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Country country = db.Countries.Find(id);
			if (country == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CountryViewModel()
			{
				Page = Page,
				Country = country
			};

			return View(viewModel);
		}

		// POST: Countries/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id, int? Page)
		{
			Country country = db.Countries.Find(id);
			db.Countries.Remove(country);
			db.SaveChanges();
			return RedirectToAction("Index", new { Page = Page });
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
