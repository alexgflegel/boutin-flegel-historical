﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Routing;
using System.Web.Mvc;

namespace BoutinFlegel.Areas.Catalog.Models
{
	public class CurrencyDivisionIndexViewModel : CurrencyDivisionModel.BaseViewModel
	{
		public IPagedList<CurrencyDivision> CurrencyDivisions { get; set; }

	}

	public class CurrencyDivisionViewModel : CurrencyDivisionModel.BaseViewModel
	{
		public CurrencyDivision CurrencyDivision { get; set; }

		public IEnumerable<SelectListItem> Countries { get; set; }
		public IEnumerable<SelectListItem> CountryDivisions { get; set; }
		public IEnumerable<SelectListItem> Currencies { get; set; }
	}

	namespace CurrencyDivisionModel
	{
		public class BaseViewModel
		{
			public InventorySearchModel InventorySearchModel { get; set; }

			public InventorySearchModel.Sort InventorySort { get; set; }

			public int? Page { get; set; }

			public RouteValueDictionary RouteValues(RouteValueDictionary ExternalValues)
			{
				return InventorySearchModel.RouteValues.Extend(ExternalValues);
			}
		}
	}
}