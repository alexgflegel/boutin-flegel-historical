﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.CountryDivision")]
	[DisplayName("Country Division")]
	public partial class CountryDivision
	{
		public CountryDivision()
		{
			Currencies = new HashSet<Currency>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CountryDivisionId { get; set; }

		[Required]
		[Display(Name = "Country")]
		public int CountryId { get; set; }

		[Value]
		[Required]
		[StringLength(100)]
		[Display(Name = "Country Division Name")]
		public string Name { get; set; }

		[StringLength(100)]
		public string Localization { get; set; }

		[Column(TypeName = "date")]
		[Display(ShortName = "Confed.")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime? Confederation { get; set; }

		[Column(TypeName = "date")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		[Display(ShortName = "Disint.")]
		public DateTime? Disintegration { get; set; }

		[JsonIgnore]
		public virtual Country Country { get; set; }

		[JsonIgnore]
		public virtual ICollection<Currency> Currencies { get; set; }
	}
}
