﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.CurrencyDivision")]
	[DisplayName("Currency Division")]
	public partial class CurrencyDivision
	{
		public CurrencyDivision()
		{
			Inventories = new HashSet<Inventory>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CurrencyDivisionId { get; set; }

		[Required]
		[Display(Name = "Currency")]
		public int CurrencyId { get; set; }

		[Value]
		[Required]
		[StringLength(50)]
		[Display(Name = "Name (Singular)", ShortName = "Name")]
		public string NameSingular { get; set; }

		[Required]
		[StringLength(50)]
		[Display(Name = "Name (Plural)", ShortName = "Name")]
		public string NamePlural { get; set; }

		[StringLength(50)]
		[Display(Name = "Localization (Singular)", ShortName = "Localization")]
		public string LocalizationSingular { get; set; }

		[StringLength(50)]
		[Display(Name = "Localization (Plural)", ShortName = "Localization")]
		public string LocalizationPlural { get; set; }

		[StringLength(5)]
		public string Symbol { get; set; }

		public int? Division { get; set; }

		[JsonIgnore]
		public Currency Currency { get; set; }

		[JsonIgnore]
		public ICollection<Inventory> Inventories { get; set; }
	}
}
