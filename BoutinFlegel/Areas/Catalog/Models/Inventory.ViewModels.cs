﻿namespace BoutinFlegel.Areas.Catalog.Models
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using PagedList;
	using System.Web.Routing;
	using System.Web.Mvc;

	public class InventoryIndexViewModel : InventoryModel.BaseViewModel
	{
		public IPagedList<Inventory> Inventories { get; set; }

		public InventorySearchModel.Sort CountrySort { get; set; }
		public InventorySearchModel.Sort CollectionSort { get; set; }
	}

	public class InventoryViewModel : InventoryModel.BaseViewModel
	{
		public Inventory Inventory { get; set; }

		public IEnumerable<SelectListItem> Countries { get; set; }
		public IEnumerable<SelectListItem> CountryDivisions { get; set; }
		public IEnumerable<SelectListItem> Currencies { get; set; }
		public IEnumerable<SelectListItem> CurrencyDivisions { get; set; }
		public IEnumerable<SelectListItem> Mediums { get; set; }
		public IEnumerable<SelectListItem> Collections { get; set; }
	}

	namespace InventoryModel
	{
		public class BaseViewModel
		{
			public InventorySearchModel InventorySearchModel { get; set; }

			public InventorySearchModel.Sort InventorySort { get; set; }

			public int? Page { get; set; }

			public RouteValueDictionary RouteValues(RouteValueDictionary ExternalValues)
			{
				return InventorySearchModel.RouteValues.Extend(ExternalValues);
			}
		}
	}
}