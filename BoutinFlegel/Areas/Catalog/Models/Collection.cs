﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.Collection")]
	[DisplayName("Collection")]
	public partial class Collection
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CollectionId { get; set; }

		[Value]
		[Required]
		[StringLength(50)]
		[Display(Name = "Collection Name")]
		public string Name { get; set; }

		[StringLength(50)]
		public string Localization { get; set; }

		[StringLength(200)]
		public string Description { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		[JsonIgnore]
		public virtual ICollection<Inventory> Inventories { get; set; }
	}
}
