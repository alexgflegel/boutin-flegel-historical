﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.Inventory")]
	[DisplayName("Inventory")]
	public partial class Inventory
	{
		public Inventory()
		{
			InventoryLots = new HashSet<InventoryLot>();
		}

		public const string BindString = "inventoryID,collectionID,typeID,currencyDivisionID,denomination,denominationAlternate,year,speccialIssue,description,quantity,localization";

		public enum Medium
		{
			Coin = 1,
			Banknote = 2,
			CoinSouvenier = 3,
			BanknoteSouvenier = 4
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int InventoryId { get; set; }

		public int? CollectionId { get; set; }

		[Required]
		[Display(Name = "Medium")]
		[Column("TypeId")]
		public Medium? TypeId { get; set; }

		[Required]
		[Display(Name = "Currency Division")]
		public int CurrencyDivisionId { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:#,##0.##}")]
		[Display(Name = "Denomination (Numerical)", ShortName = "Denomination")]
		public decimal? Denomination { get; set; }

		[StringLength(50)]
		[Display(Name = "Denomination (Text)")]
		public string DenominationAlternate { get; set; }

		public int? Year { get; set; }

		[StringLength(50)]
		[Display(Name = "Special Issue")]
		public string SpecialIssue { get; set; }

		[StringLength(50)]
		public string Description { get; set; }

		[StringLength(50)]
		public string Localization { get; set; }

		[JsonIgnore]
		public virtual Collection Collection { get; set; }

		[JsonIgnore]
		[Display(Name = "Currency Division")]
		public CurrencyDivision CurrencyDivision { get; set; }

		[JsonIgnore]
		[Display(Name = "Inventory Lots", ShortName = "Lots")]
		public ICollection<InventoryLot> InventoryLots { get; set; }
	}
}
