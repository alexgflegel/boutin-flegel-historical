﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Routing;
using System.Web.Mvc;

namespace BoutinFlegel.Areas.Catalog.Models
{
	public class CountryDivisionIndexViewModel : CountryDivisionModel.BaseViewModel
	{
		public IPagedList<CountryDivision> CountryDivisions { get; set; }
	}

	public class CountryDivisionViewModel : CountryDivisionModel.BaseViewModel
	{
		public CountryDivision CountryDivision { get; set; }

		public IEnumerable<SelectListItem> Countries { get; set; }
	}

	namespace CountryDivisionModel
	{
		public class BaseViewModel
		{
			public int? Page { get; set; }
		}
	}
}