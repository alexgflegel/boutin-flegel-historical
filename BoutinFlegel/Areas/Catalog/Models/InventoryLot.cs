﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.InventoryLot")]
	[DisplayName("Inventory Lot")]
	public partial class InventoryLot
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int InventoryLotId { get; set; }

		[StringLength(5)]
		public string Grade { get; set; }

		[Required]
		[Display(Name = "Inventory")]
		public int InventoryId { get; set; }

		public bool? Verified { get; set; }

		public int? Year { get; set; }

		[JsonIgnore]
		public virtual Inventory Inventory { get; set; }
	}
}
