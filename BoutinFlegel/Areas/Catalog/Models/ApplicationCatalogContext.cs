namespace BoutinFlegel.Areas.Catalog.Models
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class ApplicationCatalogContext : DbContext
	{
		public ApplicationCatalogContext() : base("name=ApplicationContext") { }

		public virtual DbSet<Collection> Collections { get; set; }
		public virtual DbSet<Country> Countries { get; set; }
		public virtual DbSet<CountryDivision> CountryDivisions { get; set; }
		public virtual DbSet<Currency> Currencies { get; set; }
		public virtual DbSet<CurrencyDivision> CurrencyDivisions { get; set; }
		public virtual DbSet<Inventory> Inventories { get; set; }
		public virtual DbSet<InventoryLot> InventoryLots { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Collection>()
				.HasMany(e => e.Inventories)
				.WithOptional(e => e.Collection);

			modelBuilder.Entity<Inventory>()
				.HasMany(e => e.InventoryLots)
				.WithRequired(e => e.Inventory)
				.WillCascadeOnDelete(false);
		}
	}
}
