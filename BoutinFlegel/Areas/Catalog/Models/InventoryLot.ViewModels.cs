﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Routing;
using System.Web.Mvc;

namespace BoutinFlegel.Areas.Catalog.Models
{
	public class InventoryLotIndexViewModel : InventoryLotModel.BaseViewModel
	{
		public IPagedList<InventoryLot> InventoryLots { get; set; }
	}

	public class InventoryLotViewModel : InventoryLotModel.BaseViewModel
	{
		public InventoryLot InventoryLot { get; set; }

		public IEnumerable<SelectListItem> Countries { get; set; }
		public IEnumerable<SelectListItem> CountryDivisions { get; set; }
		public IEnumerable<SelectListItem> Currencies { get; set; }
		public IEnumerable<SelectListItem> CurrencyDivisions { get; set; }
		public IEnumerable<SelectListItem> Inventories { get; set; }
		public IEnumerable<SelectListItem> Mediums { get; set; }
		public IEnumerable<SelectListItem> Collections { get; set; }
	}

	namespace InventoryLotModel
	{
		public class BaseViewModel
		{
			public int? Page { get; set; }
		}
	}
}