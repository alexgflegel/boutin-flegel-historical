﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Routing;
using System.Web.Mvc;

namespace BoutinFlegel.Areas.Catalog.Models
{
	public class CountryIndexViewModel : CountryModel.BaseCountryModel
	{
		public IPagedList<Country> Countries { get; set; }
	}

	public class CountryViewModel : CountryModel.BaseCountryModel
	{
		public Country Country { get; set; }
	}

	namespace CountryModel
	{
		public class BaseCountryModel
		{
			public int? Page { get; set; }
		}
	}
}