﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Routing;
using System.Web.Mvc;

namespace BoutinFlegel.Areas.Catalog.Models
{
	public class CurrencyIndexViewModel : CurrencyModel.BaseViewModel
	{
		public IPagedList<Currency> Currencies { get; set; }
	}

	public class CurrencyViewModel : CurrencyModel.BaseViewModel
	{
		public Currency Currency { get; set; }

		public IEnumerable<SelectListItem> Countries { get; set; }
		public IEnumerable<SelectListItem> CountryDivisions { get; set; }
	}

	namespace CurrencyModel
	{
		public class BaseViewModel
		{
			public int? Page { get; set; }
		}
	}
}