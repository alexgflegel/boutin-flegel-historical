﻿using System.Linq;
using System.Web.Routing;

namespace BoutinFlegel.Areas.Catalog.Models
{
	/// <summary>
	/// Manages the routing values and filtering for the inventory search
	/// </summary>
	public class InventorySearchModel : BaseSearchModel
	{
		public string RawString { get; set; }

		public enum Sort
		{
			NameAscending,
			NameDescending,
			CollectionAscending,
			CollectionDescending
		}

		public const string BindString = "RawString";

		/// <summary>
		/// Returns the RouteValueDictionary for all parts of the search object
		/// </summary>
		public override RouteValueDictionary RouteValues => new RouteValueDictionary { [nameof(RawString)] = RawString };

		/// <summary>
		/// Filters a dataset for each of the search models parameters
		/// </summary>
		/// <param name="inventory"></param>
		public void Filter(ref IQueryable<Inventory> inventory)
		{
			if (string.IsNullOrWhiteSpace(RawString))
				return;

			foreach (var token in BuildKeywords(RawString))
			{
				if (string.IsNullOrWhiteSpace(token.Value))
				{
					inventory = inventory.Where(s =>

					s.CurrencyDivision.Currency.CountryDivision.Country.Name.Contains(token.Key) ||
					s.CurrencyDivision.Currency.CountryDivision.Name.Contains(token.Key) ||
					s.CurrencyDivision.Currency.Name.Contains(token.Key) ||
					s.CurrencyDivision.NameSingular.Contains(token.Key) ||
					s.Description.Contains(token.Key) ||
					s.SpecialIssue.Contains(token.Key) ||
					s.Denomination.ToString().Contains(token.Key) ||
					s.Year.ToString().Contains(token.Key));
				}
				else if (token.Value.ToLower() == "country")
					inventory = inventory.Where(s => s.CurrencyDivision.Currency.CountryDivision.Country.Name.Contains(token.Key));

				else if (token.Value.ToLower() == "countrydivision")
					inventory = inventory.Where(s => s.CurrencyDivision.Currency.CountryDivision.Name.Contains(token.Key));

				else if (token.Value.ToLower() == "currency")
					inventory = inventory.Where(s => s.CurrencyDivision.Currency.Name.Contains(token.Key));

				else if (token.Value.ToLower() == "currencydivision")
					inventory = inventory.Where(s => s.CurrencyDivision.NameSingular.Contains(token.Key));

				else if (token.Value.ToLower() == "description")
					inventory = inventory.Where(s => s.Description.Contains(token.Key) || s.SpecialIssue.Contains(token.Key));

				else if (token.Value.ToLower() == "collection")
					inventory = inventory.Where(s => s.Collection.Name.Contains(token.Key));

				else if (token.Value.ToLower() == "year")
					inventory = inventory.Where(s => s.Year.ToString().Contains(token.Key));

				else if (token.Value.ToLower() == "denomination")
					inventory = inventory.Where(s => s.Denomination.ToString().Contains(token.Key));

			}
		}
	}
}
