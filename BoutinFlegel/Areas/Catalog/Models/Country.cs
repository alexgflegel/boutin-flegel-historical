﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.Country")]
	[DisplayName("Country")]
	public partial class Country
	{
		public Country()
		{
			CountryDivisions = new HashSet<CountryDivision>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CountryId { get; set; }

		[StringLength(10)]
		[Display(Name = "Country Code")]
		public string CountryCode { get; set; }

		[Value]
		[Required]
		[StringLength(50)]
		[Display(Name = "Country Name")]
		public string Name { get; set; }

		[StringLength(50)]
		public string Localization { get; set; }

		[JsonIgnore]
		[Display(Name = "Country Divisions")]
		public ICollection<CountryDivision> CountryDivisions { get; set; }
	}
}
