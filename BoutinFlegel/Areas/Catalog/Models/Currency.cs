﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BoutinFlegel.Areas.Catalog.Models
{
	[Table("coinCatalog.Currency")]
	[DisplayName("Currency")]
	public partial class Currency
	{
		public Currency()
		{
			CurrencyDivisions = new HashSet<CurrencyDivision>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Display(Name = "Currency")]
		public int CurrencyId { get; set; }

		[Required]
		[Display(Name = "Country Division")]
		public int CountryDivisionId { get; set; }

		[Value]
		[Required]
		[StringLength(50)]
		[Display(Name = "Currency Name")]
		public string Name { get; set; }

		[StringLength(50)]
		public string Localization { get; set; }

		[StringLength(5)]
		public string Symbol { get; set; }

		[StringLength(5)]
		public string Abbreviation { get; set; }

		[Column(TypeName = "date")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime? Introduced { get; set; }

		[Column(TypeName = "date")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime? Replaced { get; set; }

		[JsonIgnore]
		[Display(Name = "Country Division")]
		public CountryDivision CountryDivision { get; set; }

		[JsonIgnore]
		[Display(Name = "Currency Divisions")]
		public ICollection<CurrencyDivision> CurrencyDivisions { get; set; }
	}
}
