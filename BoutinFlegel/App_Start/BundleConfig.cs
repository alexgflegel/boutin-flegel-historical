﻿using System.Web;
using System.Web.Optimization;

namespace BoutinFlegel
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/editing-bundle").Include(
					   "~/Scripts/tinymce/tinymce.js",
						"~/Scripts/Prettify/run_prettify.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval-bundle").Include(
						"~/Scripts/jquery.validate*"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr-bundle").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap-bundle").Include(
					  "~/Scripts/tether/tether.js",
					  "~/Scripts/umd/popper.js",
					  "~/Scripts/umd/popper-utils.js",
					  "~/Scripts/bootstrap.js",
					  "~/Scripts/respond.js",
					  "~/Scripts/jquery-{version}.js",
					  "~/Scripts/select2.js",
					  "~/Scripts/BoutinFlegel.js"));

			bundles.Add(new StyleBundle("~/bundles/styling-bundle").Include(
					"~/Content/bootstrap.css",
					"~/Content/css/select2.css",
					"~/Content/materialdesignicons.css",
					"~/Content/Prettify/prettify.css",
					"~/Content/BoutinFlegel.css"
					));
		}
	}
}
