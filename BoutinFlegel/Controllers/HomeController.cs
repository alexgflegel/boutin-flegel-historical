﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoutinFlegel.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			//https://pre07.deviantart.net/3b35/th/pre/f/2011/180/9/d/research_ship_by_smirnovartem-d3kgi2t.jpg

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		public ActionResult TravelCalendar()
		{
			//calendar string diag breakdown
			ViewBag.CalendarURL = string.Join("&", new string[] { "https://calendar.google.com/calendar/embed?src=7t4496kbqn926tjv8tn32h44c8%40group.calendar.google.com",
			"showTitle=0",
			"showNav=1",
			"showPrint=0",
			"showTabs=0",
			"showCalendars=0",
			"showTz=0",
			"height=300",
			"width=300",
			"wkst=1",
			"bgcolor=%23FFFFFF",
			"ctz=America%2FRegina" });

			return PartialView("_TravelCalendarPartial");
		}

		[Authorize(Roles = nameof(Roles.BlogAdmin))]
		public ActionResult HTMLResume()
		{
			ViewBag.Item = "No Item";

			ViewBag.Resume = ResumeViewbag();


			return View("Resume");
		}

		private dynamic ResumeViewbag()
		{
			dynamic resume = new System.Dynamic.ExpandoObject();

			resume.Education = new System.Dynamic.ExpandoObject();
			resume.Education.Degree = new System.Dynamic.ExpandoObject();
			resume.ComputerSkills = new System.Dynamic.ExpandoObject();
			resume.ComputerSkills.Skills = new System.Dynamic.ExpandoObject();
			resume.ComputerSkills.Languages = new System.Dynamic.ExpandoObject();
			resume.ComputerSkills.Frameworks = new System.Dynamic.ExpandoObject();
			resume.WorkExperience = new System.Dynamic.ExpandoObject();
			resume.WorkExperience.Omnilogic = new System.Dynamic.ExpandoObject();
			resume.WorkExperience.Celero = new System.Dynamic.ExpandoObject();

			resume.Education.Title = "Education";
			resume.Education.Degree.Body = "Bachelor of Science in Computer Science, University of Regina";

			resume.ComputerSkills.Title = "Computer Skills";
			resume.ComputerSkills.Skills.Title = "Skills";
			resume.ComputerSkills.Skills.Body = "Application planning and development; software architecture; database design and interaction; reporting services; advanced MS Office; web development; client interaction";
			resume.ComputerSkills.Languages.Title = "Languages";
			resume.ComputerSkills.Languages.Body = new string[] { "C++", "C#", "Java", "JavaScript", "ColdFusion", "Visual Basic", "HTML", "PHP", "MySQL", "T-SQL", "UML Standards" };
			resume.ComputerSkills.Frameworks.Title = "Frameworks";
			resume.ComputerSkills.Frameworks.Body = new string[] { "Microsoft.NET", "ASP.NET", "WordPress", "ColdFusion", "OpenGL" };

			//real work
			resume.WorkExperience.Title = "Work Experience";
			resume.WorkExperience.Omnilogic.JobTitle = "Full Stack Developer";
			resume.WorkExperience.Omnilogic.Company = "Omnilogic Systems Inc.";
			resume.WorkExperience.Omnilogic.Dates = new string[] { new DateTime(2013, 05, 01).ToString("yyyy-MM") };
			resume.WorkExperience.Omnilogic.Body = "Supporting and developing web technologies within a wide range of technologies and languages. For example, ColdFusion and .NET applications; PHP, and WordPress.";

			resume.WorkExperience.Celero.JobTitle = "Support Analyst";
			resume.WorkExperience.Celero.Company = "Omnilogic Systems Inc.";
			resume.WorkExperience.Celero.Dates = new string[] { new DateTime(2012, 06, 01).ToString("yyyy-MM"), new DateTime(2012, 09, 10).ToString("yyyy-MM") };
			resume.WorkExperience.Celero.Body = "Supporting a critical process for credit card issuance, developing software patches, creating support and technical documentation for the process.";

			//coop work
			//resume.WorkExperience.Place.JobTitle = "";
			//resume.WorkExperience.Place.Company = "";
			//resume.WorkExperience.Place.Dates = new DateTime[] { };
			//resume.WorkExperience.Place.Body = "";

			return resume;
		}
	}
}