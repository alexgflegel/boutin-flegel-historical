function onEdit(event) {
  var columnWatch,
      columnStamp,
      columnIndex,
      rowStart,
      rowEnd,
      currentSheet,
      insertValue;

  //ignore the header rows and errors
  if(event.range.getRow() < 2)
    return;

  //find and configure the controls for each sheet with the controls
  switch(event.source.getActiveSheet().getName()){

      //fill in the name of the sheet you want the script to work on e.g. 'Sheet1'
      //one to one mapping of watching columns to stamping columns
    case 'Encounter Shares':
      columnWatch = [2];
      columnStamp = [3];
      insertValue = "1";
      break;
    case 'Parties':
      columnWatch = [1];
      columnStamp = [3];
      insertValue = new Date();
      break;
    default:
      //no mapping, exit
      return;
      break;
  }

  rowStart = event.range.getRow();
  rowEnd = event.range.getLastRow();
  columnIndex = columnWatch.indexOf(event.range.columnStart);

  //error out if the index is unknown
  if (rowStart == -1 || rowEnd == -1 || columnIndex == -1)
    return;

  currentSheet = event.source.getActiveSheet();

  //loop through each of the altered rows
  for(var i = rowStart; i <= rowEnd; i++)
  {
    var stampRange = currentSheet.getRange(i, columnStamp[columnIndex]);
    if(!currentSheet.getRange(i, columnWatch[columnIndex]).isBlank())
    {
      //set the value to the current date if the field is not blank

      //Logger.log(i + "," + columnStamp[columnIndex]);
      stampRange.setValue(insertValue);
    }
    else
    {
      //if the field is blank insert a blank
      stampRange.setValue(null);
    }
  }
 }