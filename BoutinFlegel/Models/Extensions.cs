﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BoutinFlegel
{
	public enum Roles
	{
		BlogAdmin,
		CatalogAdmin,
		SiteAdmin
	}

	public enum SiteAreas
	{
		Catalog,
		Blog
	}

	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class ValueAttribute : Attribute { }

	/// <summary>
	/// Manages the routing values and filtering for the inventory search
	/// </summary>
	public static class Extensions
	{
		/// <summary>
		/// Returns a specific attribute for an enumerator.  Returns null on default.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		/// <returns></returns>
		public static T GetAttribute<T>(this Enum value) => value.GetAttributes<T>().FirstOrDefault();

		/// <summary>
		/// Returns all custom attributes for an enumerator
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		/// <returns></returns>
		public static IEnumerable<T> GetAttributes<T>(this Enum value)
		{
			var info = value.GetType();
			var memInfo = info.GetMember(value.ToString());

			return memInfo.Count() > 0 ? memInfo[0].GetCustomAttributes(typeof(T), false).Cast<T>() : new List<T>();
		}

		public static KeyValuePair<string, string> GetDataFields<T>()
		{
			var properties = typeof(T).GetProperties();

			// Get the PropertyInfo object:
			var keyProperty = properties.FirstOrDefault(p => p.GetCustomAttributes(false).Any(a => a.GetType() == typeof(KeyAttribute)));
			var valueProperty = properties.FirstOrDefault(p => p.GetCustomAttributes(false).Any(a => a.GetType() == typeof(ValueAttribute)));

			try
			{
				return new KeyValuePair<string, string>(keyProperty.Name, valueProperty.Name);
			}
			catch
			{
				throw new Exception(string.Format("{0} does not Contain both a KeyAttribute and a ValueAttribute.", typeof(T).Name));
			}
		}

		public static SelectList SelectList<T>(IEnumerable items, object selectedValue = null)
		{
			var displayValues = GetDataFields<T>();

			return new SelectList(items, displayValues.Key, displayValues.Value, selectedValue);
		}

		/// <summary>
		/// Extends the RouteValueDictionary by attaching all items to an external dictionary
		/// </summary>
		/// <param name="externalValues"></param>
		/// <returns></returns>
		public static RouteValueDictionary Extend(this RouteValueDictionary internalValues, RouteValueDictionary externalValues)
		{
			foreach (var routeValue in (internalValues ?? new RouteValueDictionary()))
				externalValues[routeValue.Key] = routeValue.Value;

			return externalValues;
		}


		/// <summary>
		/// Returns null if the string is null or empty for null coalescence
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string NullIfWhiteSpace(this string value) => string.IsNullOrWhiteSpace(value) ? null : value;

		/// <summary>
		/// Creates a label for the groupname
		/// </summary>
		/// <typeparam name="TModel"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="helper"></param>
		/// <param name="expression"></param>
		/// <returns></returns>
		public static IHtmlString DisplayGroupNameFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
		{
			var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
			var content = string.Empty;

			if (metadata.AdditionalValues.ContainsKey("GroupName"))
				content = metadata.AdditionalValues["GroupName"].ToString();

			return new HtmlString(content);
		}

		public static IHtmlString DisplayShortNameFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
		{
			var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
			var content = metadata.ShortDisplayName ?? metadata.DisplayName ?? string.Empty;
			return new HtmlString(content);
		}

		/// <summary>
		/// Creates an html tag for a label to display custom data.
		/// </summary>
		/// <typeparam name="TModel"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="helper"></param>
		/// <param name="expression"></param>
		/// <param name="labelText"></param>
		/// <param name="htmlAttributes"></param>
		/// <returns></returns>
		public static IHtmlString ShortLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string labelText, object htmlAttributes)
		{
			var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
			var content = metadata.ShortDisplayName ?? metadata.DisplayName ?? labelText.NullIfWhiteSpace() ?? string.Empty;

			var builder = new TagBuilder("label");

			builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			builder.Attributes.Add("for", metadata.PropertyName);
			builder.SetInnerText(content);

			return new HtmlString(builder.ToString(TagRenderMode.Normal));
		}

		public static IHtmlString HtmlActionLink<TModel>(this HtmlHelper<TModel> helper, IHtmlString displayText, string actionName, RouteValueDictionary routeValues, object htmlAttributes)
		{
			var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

			var builder = new TagBuilder("a");
			builder.Attributes.Add("href", routeValues == null
					? urlHelper.Action(actionName)
					: urlHelper.Action(actionName, routeValues));
			//builder.MergeAttributes(routeValues);
			builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			builder.InnerHtml = displayText.ToString();
			return new HtmlString(builder.ToString(TagRenderMode.Normal));
		}

		public static IHtmlString Icon<TModel>(this HtmlHelper<TModel> helper, object htmlAttributes)
		{
			var builder = new TagBuilder("i");
			builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			return new HtmlString(builder.ToString(TagRenderMode.Normal));
		}
	}

	public class BaseSearchModel
	{

		public const string TokenExpression = @"([a-zA-Z]+:(?:\([^)]+?\)|[^( ]+))";
		public virtual RouteValueDictionary RouteValues { get; }

		protected List<KeyValuePair<string, string>> BuildKeywords(string input)
		{
			var keywordTokens = new List<KeyValuePair<string, string>>();

			//fetch any tokens in the format keyword:word or keyword:(multiple words)
			var keywords = Regex.Matches(input, TokenExpression)
								.Cast<Match>()
								.Select(m => m.Value)
								.ToList();

			//remove the previous matches and split out the remaining words or phrases
			var regularWords = Regex.Matches(Regex.Replace(input, TokenExpression, string.Empty).Trim(), @"[\""].+?[\""]|[^ ]+")
								.Cast<Match>()
								.Select(m => m.Value.Replace("\"", string.Empty))
								.ToList();

			//add the words to the return
			foreach (var word in regularWords)
			{
				keywordTokens.Add(new KeyValuePair<string, string>(word, string.Empty));
			}

			//retrieve the inner words and add them to the return
			foreach (var keyword in keywords)
			{
				var split = keyword.IndexOf(":");
				var key = keyword.Substring(0, split);
				var word = keyword.Substring(split + 1);

				//ignore the brackets
				if (word.StartsWith("(") && word.EndsWith(")"))
					word = word.Substring(1, word.Length - 2);

				keywordTokens.Add(new KeyValuePair<string, string>(word, key));
			}

			return keywordTokens;
		}
	}

	public class DataAnnotationsExtensionModelMetadataProvider : DataAnnotationsModelMetadataProvider
	{
		protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
		{

			var metadata = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
			// Prefer [Display(Name="")] to [DisplayName]
			var display = attributes.OfType<DisplayAttribute>().FirstOrDefault();
			if (display != null)
			{
				metadata.DisplayName = display.GetName().NullIfWhiteSpace() ?? metadata.DisplayName;
				metadata.AdditionalValues.Add(nameof(display.GroupName), display.GetGroupName());
				metadata.AdditionalValues.Add(nameof(display.Prompt), display.GetPrompt());
			}

			// Prefer [Editable] to [ReadOnly]
			var editable = attributes.OfType<EditableAttribute>().FirstOrDefault();
			if (editable != null)
				metadata.IsReadOnly = !editable.AllowEdit;

			// If [DisplayFormat(HtmlEncode=false)], set a data type name of "Html"
			// (if they didn't already set a data type)
			var displayFormat = attributes.OfType<DisplayFormatAttribute>().FirstOrDefault();

			if (displayFormat != null && !displayFormat.HtmlEncode && string.IsNullOrWhiteSpace(metadata.DataTypeName))
				metadata.DataTypeName = DataType.Html.ToString();

			return metadata;
		}
	}
}
