﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(BoutinFlegel.Startup))]
namespace BoutinFlegel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
