﻿
var BoutinFlegel = class BoutinFlegel {
	/**
	* Handles various object initialization
	*/

	static Initialize() {
		//$('[data-toggle="tooltip"]').tooltip();
		//$('[data-toggle="popover"]').popover();

		this.CorrectPagination()
	}

	/**
	 * Uses jQuery to monitor the watchObject for a change and executes an Ajax call to fetch a new list of options.
	 * If successful the updateObject will be cleared and the new options added to the select control.
	 *
	 * @param {string} watchObject  todo.
	 * @param {string} updateObject  todo.
	 * @param {string} url  todo.
	 */
	static AjaxSelectRefresh(watchObject, updateObject, url) {
		//create the change watch
		$(watchObject).change(function () {
			$.ajax({
				url: url + $(this).val(),
				type: "POST",
				success: function (data) {
					BoutinFlegel.UpdateSelectData(updateObject, data);
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					BoutinFlegel.UpdateSelectData(updateObject, "");
				}
			});
			return false;
		});
	}

	/**
	 * @param {string} updateObject todo.
	 * @param {string} data todo.
	 */
	static UpdateSelectData(updateObject, data) {

		//clear and insert the new values
		$(updateObject).empty();

		//insert a dummy display value
		$(updateObject).append($("<option/>", {
			value: "",
			text: "Please Select"
		}));

		//insert the rest of the data below
		$.each(data, function (index, value) {
			$(updateObject).append($("<option/>", {
				value: value.Value,
				text: value.Text
			}));
		});

		var test = $(updateObject);

		//select the display value
		$(updateObject).val("").trigger("change");
	}

	static CorrectPagination() {
		$('ul.pagination').each(function () {
			// this is inner scope, in reference to the .phrase element
			$(this).find('li').each(function () {
				// cache jquery var
				$(this).addClass("page-item");
				$(this).find('a').each(function () {
					$(this).addClass("page-link")
				});
			});
		});
	}

	static LaunchSelect2() {
		$("select.select2-jquery-hook").select2();
	}

	static LaunchTinyMCE() {

		tinymce.baseURL = "/Scripts/tinymce";

		// Initialize tinyMCE Editor with your preferred option
		//more options and examples can be found at https://www.tinymce.com/docs/demo/
		tinymce.init({
			selector: "textarea.editor-jquery-hook",
			height: 500,
			plugins: [
				"textpattern"
			],
			textpattern_patterns: [
				{ start: '*', end: '*', format: 'italic' },
				{ start: '**', end: '**', format: 'bold' },
				{ start: '#', format: 'h1' },
				{ start: '##', format: 'h2' },
				{ start: '###', format: 'h3' },
				{ start: '####', format: 'h4' },
				{ start: '#####', format: 'h5' },
				{ start: '######', format: 'h6' },
				{ start: '1. ', cmd: 'InsertOrderedList' },
				{ start: '* ', cmd: 'InsertUnorderedList' },
				{ start: '- ', cmd: 'InsertUnorderedList' }
			],

			//toolbar1: "alignleft aligncenter alignright alignjustify | outdent indent blockquote | fontselect fontsizeselect",
			//toolbar2: "bullist numlist | undo redo | code preview | ltr rtl | visualchars visualblocks | forecolor backcolor",

			visualblocks_default_state: true,
			end_container_on_empty_block: true,
			content_css: [
				"/bundles/styling-bundle"
			]
		});
	}
};
