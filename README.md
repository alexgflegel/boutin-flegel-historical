# README #

### What is this repository for? ###

* Hosting the Coin Management App
* A simple blog & other contact information

### How do I get set up? ###

* Visual Studio 2015 (Administrative Mode)
* SQL Server 2014 (Or Newer)
* IIS 10
* NuGet is configured in this project to grab a number of libraries and packages
* A database backup is provided within the app data folder

### Contribution guidelines ###

* Theming using [Bootstrap](http://go.microsoft.com/fwlink/?LinkID=615519)
* Learn [Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Other guidelines

### Who do I talk to? ###

* Alexander Boutin Flegel [alexander.flegel@omnilogic.net]

## Microsoft Links ##

### Customization ###

* Get started with [ASP.NET MVC](http://go.microsoft.com/fwlink/?LinkID=615541)
* Change the site's [theme](http://go.microsoft.com/fwlink/?LinkID=615523)
* Add more libraries using [NuGet](http://go.microsoft.com/fwlink/?LinkID=615524)
* Configure [authentication](http://go.microsoft.com/fwlink/?LinkID=615542)
* Customize [information](http://go.microsoft.com/fwlink/?LinkID=615526) about the website users
* Add HTTP services using [ASP.NET Web API](http://go.microsoft.com/fwlink/?LinkID=615528)
* [Secure](http://go.microsoft.com/fwlink/?LinkID=615529) your web API
* Add real-time web with ASP.NET [SignalR](http://go.microsoft.com/fwlink/?LinkID=615530)
* Add components using [Scaffolding](http://go.microsoft.com/fwlink/?LinkID=615531)
* Test your app with [Browser Link](http://go.microsoft.com/fwlink/?LinkID=615532)
* [Share](http://go.microsoft.com/fwlink/?LinkID=615533) your project


### Deploy ###

* Ensure your app is [ready for production](http://go.microsoft.com/fwlink/?LinkID=615534)
* [Microsoft Azure](http://go.microsoft.com/fwlink/?LinkID=615535)
* [Hosting providers](http://go.microsoft.com/fwlink/?LinkID=615536)


### Help ###

* [Get help](http://go.microsoft.com/fwlink/?LinkID=615537)
* [Get more templates](http://go.microsoft.com/fwlink/?LinkID=615538)
